(load "project/qq.scm")

;;===================================================================================================================================
;;====================================================== Utility definitions: =======================================================
;;===================================================================================================================================
(define *reserved-words*
'(and begin cond define do else if lambda
	let let* letrec or quasiquote unquote
		unquote-splicing quote set!))

;; Constant checker that includes the void constant and is used in the main parser
(define constant?
	(lambda (sexpr)
		;(display "constant? sexpr: ")
		;(display sexpr)
		;(newline)
		(or (eq? (void) sexpr) (const? sexpr) (vector? sexpr) (null? sexpr))))

;; Checker that is used in the main parser
(define var? 
	(lambda (sexpr)
		;(display "var? sexpr: ")
		;(display sexpr)
		;(newline)
		(not (ormap (lambda (res-word) 
			(eq? res-word sexpr)) *reserved-words*))))

;; For lambda usage
(define improper-list-last-element
	(lambda (lst)
		(cond
			((not (pair? lst)) lst)
			(else (improper-list-last-element (cdr lst))))))

;; For lambda usage
(define improper-list-except-last-element
	(lambda (lst)
		(cond
			((not (pair? lst)) '())
			(else (cons (car lst) (improper-list-except-last-element (cdr lst)))))))

;; For Lambda usage
(define validate-paramlist
	(lambda (params)
		(cond
			((null? params) #t) 
			((= (length params) 1) #t)
			(else 
				(and 
					(fold-left (lambda (term x) (and term (not (eq? x (car params))))) #t (cdr params))
					(validate-paramlist (cdr params)))
			))))

;; For Begin usage
;; Example:     (begin a (begin b (begin c (begin d e (begin f g h) "Akuna Matata")))) 
;;          ==> (a b c d e f g h "Akuna Matata") 


;; Example:     (begin (if #t 1 2) a (if g (begin h i j) t) (lambda (x y z) (if x (begin a t s) z)))
;;          ==> ((if #t 1 2) a (if g (begin h i j) t) (lambda (x y z) (if x (begin a t s) z)))


;; '(begin a (begin 5 (begin 1 2 3) 4) (begin b (f (begin c (begin d e (begin f g h) "Akuna Matata")))))
;; (seq ((var a) (const 5) (const 1) (const 2) (const 3) (const 4) (var b) (applic (var f) ((seq ((var c) (var d) (var e) (var f) (var g) (var h) (const "Akuna Matata")))))))


;; '(begin (lambda () (begin 'a 'b) 'c) (begin 'd))
;; (seq ((lambda-simple
;;        ()
;;        (seq ((const a) (const b) (const c))))
;;       (const d)))


;; '((lambda () (begin 'a 'b) 'c) (begin 'd))

;; '(lambda () (begin 'a 'b) 'c)

;; '(() (begin 'a 'b) 'c))
(define remove-all-begins
	(lambda (lst)
		(cond
			((null? lst) lst)
			((and (list? lst) (equal? (car lst) 'begin))
				(remove-all-begins (cdr lst)))
			((and (list? lst) (list? (car lst)))
				(if (equal? (caar lst) 'begin)
					(append (remove-all-begins (car lst)) (remove-all-begins (cdr lst)))
					(append (list (car lst)) (remove-all-begins (cdr lst)))))
			(else (cons (car lst) (remove-all-begins (cdr lst)))))))

;; For let* usage
(define let*-let-expander
	(lambda (sexpr)
		(cond
			((null? (cadr sexpr))
				(cons 'let (cdr sexpr)))
			((= (length (cadr sexpr)) 1)
				(cons 'let (cdr sexpr)))
			(else
				(list 'let (list (caadr sexpr)) (let*-let-expander (append (list 'let*) (list (cdadr sexpr)) (cddr sexpr))))))))

;; For letrec usage 
(define letrec-lambda-expander
	(lambda (sexpr)
		(cond
			((null? (cadr sexpr)) 
				(list (list 'lambda '() (list (append (list 'lambda '()) (cddr sexpr))))))
			(else 
				(let ((vars (map car (cadr sexpr)))
					  (args (map cdr (cadr sexpr))))
					(append
						(list
							(append
								(list 'lambda vars)
								(map (lambda (var arg) (append (list 'set! var) arg)) vars args)
								(list (list (append (list 'lambda '()) (cddr sexpr))))))
						(map (lambda (var) '#f) vars)))))))		
;;===================================================================================================================================
;;====================================================== Utility definitions: =======================================================
;;===================================================================================================================================



;;===================================================================================================================================
;;=========================================================== Core Forms: ===========================================================
;;===================================================================================================================================


;;=========================================================== Case Const: ===========================================================
;; A constant list is of the form '(1 2 3)
;; (parse ''(1 2 3)) => (const (1 2 3))
;; A constant vector is of the form '#(1 2 3)
;; (parse ''#(1 2 3)) => (const #(1 2 3))
;; To create a list at runtime, you do (list 1 2 3)
;; (parse '(list 1 2 3)) => (applic (var list) ((const 1) (const 2) (const 3)))
;; To create a vector at runtime, you do (vector 1 2 3)
;; (parse '(vector 1 2 3)) => (applic (var vector) ((const 1) (const 2) (const 3)))

;; Predicate for constant expression
(define constant-exp?
	(lambda (sexpr)
		;(display "constant? sexpr: ")
		;(display sexpr)
		;(newline)
		(and (list? sexpr)
			(= (length sexpr) 2)
			(eq? (car sexpr) 'const))))

;; Maker for a const
(define make-constant
	(lambda (sexpr)
		;(display "make-constant sexpr: ")
		;(display sexpr)
		;(newline)
		(if (list? sexpr) (list 'const (cadr sexpr))
			(list 'const sexpr))))

;; Getter for the const
(define constant->val
	(lambda (const-sexpr)
		;(display "make-constant const-sexpr: ")
		;(display const-sexpr)
		;(newline)
		(cadr const-sexpr)))
;;=========================================================== Case Const: ===========================================================


;;========================================================== Case Variable: =========================================================
;; Info regarding ormap,andmap:
;; In pseudo-Scheme,
;; (andmap f xs)  ==  (fold and #t (map f xs))
;; (ormap  f xs)  ==  (fold or  #f (map f xs))
;; except that:
;; You can't use and and or in this way.
;; andmap and ormap can short-circuit processing the list.
;; That is, except for slightly different short-circuiting behavior,
;; (andmap f (list x1 x2 x3 ...))  ==  (and (f x1) (f x2) (f x3) ...)
;; (ormap  f (list x1 x2 x3 ...))  ==  (or  (f x1) (f x2) (f x3) ...)

;; Predicate for var expression
(define var-exp?
	(lambda (sexpr)
		;(display "constant? sexpr: ")
		;(display sexpr)
		;(newline)
		(and (list? sexpr)
			(= (length sexpr) 2)
			(eq? (car sexpr) 'var))))

;; Maker for a var expression
(define make-var 
	(lambda (sexpr)
		;(display "make-var sexpr: ")
		;(display sexpr)
		;(newline)
		(list 'var sexpr)))

;; Getter for the var
(define var->val
	(lambda (var-sexpr)
		;(display "var->val var-sexpr: ")
		;(display var-sexpr)
		;(newline)
		(cadr var-sexpr)))
;;========================================================== Case Variable: =========================================================


;;======================================================== Case Conditionals: =======================================================
;; Note: eq? checks equality of object in memory, which means it's true only if it points on the same object. 
;;		 equal? checks deep equality.

;; Predicate for conditional
(define conditional-exp? 
	(lambda (sexpr)
		(and (list? sexpr)
			(or (= (length sexpr) 4)  ;; (<if> <test> <dit> <dif>)
				(= (length sexpr) 3)) ;; (<if> <test> <dit>) (we concatinate #void.)
			(eq? (car sexpr) 'if3))))

;; Maker for a conditional
(define make-conditional 
	(lambda (test dit dif) 		
			(list 'if3 test dit dif)))

;; Getters for the conditional
(define conditional->test
	(lambda (sexpr)
		(cadr sexpr)))

(define conditional->then
	(lambda (var-sexpr)
		(caddr sexpr)))

(define conditional->else
	(lambda (var-sexpr)
		(cadddr sexpr)))

;;======================================================== Case Conditionals: =======================================================


;;======================================================== Case Disjunction: ========================================================
;;Predicate for disjunction
(define disjunction-exp? 
	(lambda (sexpr)
		;(display "var? sexpr: ")
		;(display sexpr)
		;(newline)
		(and (list? sexpr) 
			(equal? (car sexpr) 'or))))

;; Maker for a conditional
(define make-disjunction 
	(lambda (contents) 		
			(list 'or contents)))

;; Getter for the conditional
(define disjunction->contents
	(lambda (sexpr)
		(cdr sexpr)))
;;======================================================== Case Disjunction: ========================================================


;;========================================================== Case Lambda: ===========================================================
;; Predicate for lambda
(define lambda-exp? 
	(lambda (sexpr)
		;(display "var? sexpr: ")
		;(display sexpr)
		;(newline)
		(and (list? sexpr) 
			(or (equal? (car sexpr) 'lambda-simple)
				(equal? (car sexpr) 'lambda-opt))))) 

;; Maker for lambda simple
(define make-lambda-simple 
	(lambda (args body)
		;(display "args sexpr: ") (display args) (newline)
		;(display "body sexpr: ") (display body) (newline)
		(if (= (length body) 1)
			(list 'lambda-simple args (car body))
			(list 'lambda-simple args (list 'seq body)))))

;; Getters for lambda simple
(define lambda-simple->args
	(lambda (lambda-expr)
		(cadr lambda-expr)))
(define lambda-simple->body
	(lambda (lambda-expr)
		(caddr lambda-expr)))

;; Maker for lambda optional
(define make-lambda-optional 
	(lambda (args opt body)
		;(display "make-lambda-optional args: ")
		;(display args)
		;(newline)
		;(display "make-lambda-optional opt: ")
		;(display opt)
		;(newline)
		;(display "make-lambda-optional body: ")
		;(display body)
		;(newline)
		(if (= (length body) 1)
			(list 'lambda-opt args opt (car body))
			(list 'lambda-opt args opt (list 'seq body)))))

;; Getters for lambda optional
(define lambda-optional->args
	(lambda (lambda-expr)
		(cadr lambda-expr)))
(define lambda-optional->opt
	(lambda (lambda-expr)
		(caddr lambda-expr)))
(define lambda-optional->body
	(lambda (lambda-expr)
		(cadddr lambda-expr)))

;; Maker for lambda variadic
(define make-lambda-variadic 
	(lambda (opt body)
		(if (= (length body) 1)
			(list 'lambda-opt '() opt (car body))
			(list 'lambda-opt '() opt (list 'seq body)))))

;; Getters for lambda variadic
(define lambda-variadic->opt
	(lambda (lambda-expr)
		(caddr lambda-expr)))
(define lambda-variadic->body
	(lambda (lambda-expr)
		(cadddr lambda-expr)))
;;========================================================== Case Lambda: ===========================================================


;;========================================================== Case Define: ===========================================================
;; Predicate for define
(define define-exp?
	(lambda (sexpr)
		(and (list? sexpr) (equal? (car sexpr) 'define))))

;; Maker for define
(define make-define
	(lambda (var exprs)
		;(display "make-define var: ") (display var) (newline)
		;(display "make-define exprs: ") (display exprs)	(newline)
		(if (= (length exprs) 1)
			(list 'define var (car exprs))
			(list 'define var (list 'seq exprs)))))
		;(if (= (length exprs) 1)
		;	(list 'def var (car exprs))
		;	(list 'def var exprs))))
		;(list 'def var exprs)))

;; Getters for define
(define define-exp->var
	(lambda (defexp)
		(cadr defexp)))
(define define-exp->bound
	(lambda (defexp)
		(caddr defexp)))	
;;========================================================== Case Define: ===========================================================


;;======================================================== Case Assignment: =========================================================
;; Predicate for assignment
(define assignment-exp?
	(lambda (sexpr)
		(and (list? sexpr) (equal? (car sexpr) 'set))))

;; Maker for assignment
(define make-assignment
	(lambda (var assgn)
		(list 'set var assgn)))

;; Getters for assignment
(define assignment-exp->var
	(lambda (assgn-exp)
		(cadr assgn-exp)))
(define assignment-exp->assgn
	(lambda (assgn-exp)
		(caddr assgn-exp)))
;;======================================================== Case Assignment: =========================================================


;;======================================================== Case Application: ========================================================
;; Predicate for application
(define application-exp? 
	(lambda (sexpr)
		(and 
			(list? sexpr)
			(equal? (car sexpr) 'applic))))

;; Maker for application
(define make-application
	(lambda (proc args)
		(list 'applic proc args)))

;; Getters for application
(define application->proc
	(lambda (sexpr)
		(cadr sexpr)))
(define application->args
	(lambda (sexpr)
		(caddr sexpr)))	
;;======================================================== Case Application: ========================================================


;;========================================================= Case Sequence: ==========================================================
;; Predicate for sequence
(define sequence-exp? 
	(lambda (sexpr)
		(and 
			(list? sexpr)
			(equal? (car sexpr) 'seq))))

;; Maker for sequence
(define make-sequence-explic
	(lambda (seq-items)
		;(display "make-sequence-explic seq-items: ")
		;(display seq-items)
		;(newline)
		(list 'seq seq-items)))

;; Getter for sequence
(define sequence->seq-items
	(lambda (sexpr)
		(cadr sexpr)))
;;========================================================= Case Sequence: ==========================================================

;;===================================================================================================================================
;;=========================================================== Core Forms: ===========================================================
;;===================================================================================================================================



;;===================================================================================================================================
;;=========================================================== Main parser: ==========================================================
;;===================================================================================================================================
(define parse
  	(lambda (sexpr)
  		;(display "parse sexpr: ")
		;(display sexpr)
		;(newline)
  		(cond

  			;; Case Const
  			((constant? sexpr) (make-constant sexpr))

  			;; List cases
  			((list? sexpr)
  				(cond 

  					;; Case Conditional
  					((eq? (car sexpr) 'if)
  						(cond 
  							((= (length sexpr) 4)
  								(make-conditional 
  									(parse (cadr sexpr))   		;; <test>
  									(parse (caddr sexpr))  		;; <then>
  									(parse (cadddr sexpr))))	;; <else>
  							((= (length sexpr) 3)
  								(make-conditional
  									(parse (cadr sexpr))   		;; <test>
  									(parse (caddr sexpr))  		;; <then>
  									(parse `,(void))))			;; <void>
  							(else (error 'parser  (format "Unknown form: ~a" sexpr)))))

  					;; Case Disjunction
  					((eq? (car sexpr) 'or)
  						(if (= (length (cdr sexpr)) 1)
  							(parse (cadr sexpr))
  							(if (= (length (cdr sexpr)) 0)
  								(make-constant '#f)
  								(make-disjunction (map parse (cdr sexpr))))))

  					;; Case Lambda
  					((eq? (car sexpr) 'lambda)
  						(cond
  							((= (length sexpr) 1)
  								(error 'parser  (format "Unknown form: ~a" sexpr))) 
  							((list? (cadr sexpr))				;; Sub case Lambda Simple.
  								(if (validate-paramlist (cadr sexpr))                 
  									(make-lambda-simple (cadr sexpr) (map parse (remove-all-begins (cddr sexpr))))
  									(error 'parser  (format "Invalid parameter list: ~a" (cadr sexpr)))))
  							((pair? (cadr sexpr))				;; Sub case Lambda optional.
								(if (validate-paramlist 
										(improper-list-except-last-element (cadr sexpr)))
  									(make-lambda-optional 
  										(improper-list-except-last-element (cadr sexpr))
  										(improper-list-last-element (cadr sexpr))
  										(map parse (remove-all-begins (cddr sexpr))))
  									(error 'parser  (format "Invalid parameter list: ~a" (cadr sexpr)))))
  							(else 								;; Sub case Lambda Variadic. 
  								(make-lambda-variadic			;; Note: in case of tests failing, might want to change (else) to something more concrete.
  									(cadr sexpr) 
  									(map parse (remove-all-begins (cddr sexpr)))))))

  					;; Case Define
  					((eq? (car sexpr) 'define)
  						(cond 
  							((or (= (length sexpr) 1) (= (length sexpr) 2))
  								(error 'parser  (format "Unknown form: ~a" sexpr)))
  							((pair? (cadr sexpr))				;; Sub case MIT define.
  								(make-define
  									(make-var (caadr sexpr))
  									(list (parse (append (list 'lambda (cdadr sexpr)) (cddr sexpr))))))
  							(else 								;; Sub case regular define.
  								(make-define 
  									(make-var (cadr sexpr))
  									(map parse (cddr sexpr))))))

  					;; Case Assignment
  					((eq? (car sexpr) 'set!)
  						(if (= (length sexpr) 3)
  							(make-assignment
  								(make-var (cadr sexpr))
  								(parse (caddr sexpr)))
  							(error 'parser  (format "Unknown form: ~a" sexpr))))
  					
  					;; Case Sequence: Explicit
  					((eq? (car sexpr) 'begin)
  						(cond 
  							((= (length sexpr) 1)				;; '(begin) => (const #<void>)
  								(make-constant `,(void)))
  							((= (length sexpr) 2)
  								(parse (cadr sexpr)))
  							(else
  								(make-sequence-explic
  									(map parse (remove-all-begins sexpr))))))

  					;; SPOT FOR SPECIAL FORMS EXPANSIONS:

  					;; Case And
  					((eq? (car sexpr) 'and)
						(cond 
							((= (length sexpr) 1)
								(make-constant '#t))
							((= (length sexpr) 2)
								(parse (cadr sexpr)))
							(else
								(make-conditional
									(parse (cadr sexpr))
									(parse (append (list 'and) (cddr sexpr)))
									(make-constant '#f)))))

  					;; Case cond
  					((eq? (car sexpr) 'cond)
  						(cond
  							((= (length sexpr) 1)
  								;(display "1st clouse") (newline)
  								(error 'parser (format "Unknown form: ~a" sexpr)))
  							((not (list? (cadr sexpr)))
  								;(display "2nd clouse") (newline)
  								(error 'parser (format "This is not a valid cond-rib: : ~a" (cadr sexpr))))
  							((null? (cadr sexpr))
  								;(display "3rd clouse") (newline)
  								(error 'parser (format "This is not a valid cond-rib: : ~a" (cadr sexpr))))
  							((= (length (cadr sexpr)) 1)
  								;(display "4th clouse") (newline)
  								(error 'parser (format "This is not a valid cond-rib: : ~a" (cadr sexpr))))
  							(else
  								(cond
  									((eq? (caadr sexpr) 'else)
  											(parse (append (list 'begin) (cdadr sexpr))))
  									(else
  										(make-conditional
  											(parse (caadr sexpr))
  											(parse (append (list 'begin) (cdadr sexpr)))
  											(if (null? (cddr sexpr))
		  										(make-constant `,(void))
		  										(parse (append (list 'cond) (cddr sexpr))))))))))

  					;; Case let
  					((eq? (car sexpr) 'let)
  						(cond
  							((= (length sexpr) 1)
  								(error 'parser  (format "Unknown form: ~a" sexpr)))
  							((= (length sexpr) 2)
  								(error 'parser  (format "Unknown form: ~a" sexpr)))
  							((null? (cadr sexpr))
  								(make-application (make-lambda-simple '() (map parse (cddr sexpr))) '()))
  							(else
  								(let ((params (map (lambda (binding) (car binding)) (cadr sexpr)))
  										(args (map (lambda (binding) (parse (cadr binding))) (cadr sexpr))))
  									(if (validate-paramlist params)
  										(make-application (make-lambda-simple params (map parse (cddr sexpr))) args)
  										(error 'parser  (format "Invalid parameter list: ~a" params)))))))
  					
  					;; Case let*
  					((eq? (car sexpr) 'let*)
  						(cond
  							((= (length sexpr) 1)
  								(error 'parser  (format "Unknown form: ~a" sexpr)))
  							((= (length sexpr) 2)
  								(error 'parser  (format "Unknown form: ~a" sexpr)))
  							((not (list? (cadr sexpr)))
  								(error 'parser  (format "Unknown form: ~a" sexpr)))
  							(else
  								(parse (let*-let-expander sexpr)))))
  					
  					;; Case letrec
  					((eq? (car sexpr) 'letrec)
  						(cond 
  							((= (length sexpr) 1)
  								(error 'parser (format "Unknown form: ~a" sexpr)))
  							((= (length sexpr) 2)
  								(error 'parser (format "Unknown form: ~a" sexpr)))
  							((not (list? (cadr sexpr)))
  								(error 'parser (format "Unknown form: ~a" sexpr)))
  							(else 
  								(parse (letrec-lambda-expander sexpr)))))

  					;; Case quasiquote
  					((eq? (car sexpr) 'quasiquote)
  						(cond
  							((= (length sexpr) 1)
  								(error 'parser (format "Unknown form: ~a" sexpr)))
  							((> (length sexpr) 2)
 								(error 'parser (format "Unknown form: ~a" sexpr))) 								
  							(else
  								(parse (expand-qq (cadr sexpr))))))

  					;; Case Application
  					(else
  						(if (= (length sexpr) 1)
  							(make-application
  								(parse (car sexpr))
  								'())
  							(make-application 
  								(parse (car sexpr))
  								(map parse (cdr sexpr)))))
  				)) 								

			;; Case Var
  			((var? sexpr) (make-var sexpr))

  			;; Else - case parsing error
  			(else (error 'parser  (format "Unknown form: ~a" sexpr))))
  		))
;;===================================================================================================================================
;;=========================================================== Main parser: ==========================================================
;;===================================================================================================================================
